package com.testrestapidct.example.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MarriedStatus {
    private int married = 0;
    private int single = 0;
}
