package com.testrestapidct.example.controller;

import com.testrestapidct.example.dto.Debtor6Request;
import com.testrestapidct.example.service.ExampleServiceImpl;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class TestController {

    @Autowired
    ExampleServiceImpl exampleService;

    @PostMapping("/test")
    public ResponseEntity<Object> testSubmitValidation(@RequestBody @Valid  Debtor6Request debtor6RequestDTO, BindingResult bindingResult){
        return exampleService.validationTest(debtor6RequestDTO,bindingResult);
    }


}
