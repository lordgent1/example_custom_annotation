package com.testrestapidct.example.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Setter
@Getter
@Entity
public class aaTemplate  {
    @Id
    private String id;
    @Id
    @Column(name = "IDAATEMPLATE")
    private String idAaTemplate;

    @Column(name = "NAMA_DEBTOR")
    private String namaDebtor;

    @Column(name = "CIF")
    private String cif;

    @Column(name = "GCIF")
    private String gcif;

    @Column(name = "MULAI")
    private Timestamp mulai;

    @Column(name = "SELESAI")
    private Timestamp selesai;

    @Column(name = "FK_IDSTATUS")
    private Integer idStatus;

    @Column(name="FK_IDADMBU")
    private Long idAdmBu;

    @Column(name = "DELETED_DATE")
    private Timestamp deletedDate;

    @Column(name = "IDUSERJOBLIST")
    private String userLogin;

    @Column(name = "USERROLEJOBLIST")
    private String userRole;

    @Column(name = "CR")
    private String cr;

    @Column(name = "BAM")
    private String bam;

    @Column(name = "KETERANGAN")
    private String keterangan;

    @Column(name = "LAST_TRANSFER")
    private String lastTransfer;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "DEBTOR_TYPE", nullable = true)
    private Integer debtorType;
}
