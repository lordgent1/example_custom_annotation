package com.testrestapidct.example.dto;

import com.fasterxml.jackson.annotation.*;
import com.testrestapidct.example.Helper.CustomLabel;
import jakarta.validation.constraints.AssertTrue;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;




@Setter
@Getter
public class Debtor6Request {

    @NotNull(message = "Alamat Usaha Bisnis cannot be null")
    @CustomLabel("No.1 Alamat Usaha Bisnis")
    private String alamatUsahaBisnis;

    @NotNull(message = "Kelurahan cannot be null")
    @CustomLabel("No.2 Kelurahan")
    private String kelurahan;

    @NotNull(message = "Kecamatan cannot be null")
    @CustomLabel("No.3 Kecamatan")
    private String kecamatan;

    @NotNull(message = "Status Perkawinan cannot be null")
    @CustomLabel("Status Perkawinan")
    private MarriedInfo statusPerkawinan;

    @CustomLabel("Nama Pasangan")
    private String namaPasangan;


}
