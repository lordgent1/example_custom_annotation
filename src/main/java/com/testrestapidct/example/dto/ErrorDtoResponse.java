package com.testrestapidct.example.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ErrorDtoResponse {
    private Object labelName;
    private String pageName;
    private String message;
}
