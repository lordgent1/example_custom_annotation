package com.testrestapidct.example.dto;

import java.util.Collection;

public enum MarriedInfo {
    SINGLE("Single"),
    MARRIED("Married");

    private final String label;

    MarriedInfo(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

}
