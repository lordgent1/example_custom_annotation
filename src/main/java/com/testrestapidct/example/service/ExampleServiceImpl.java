package com.testrestapidct.example.service;

import com.testrestapidct.example.Helper.CustomLabel;
import com.testrestapidct.example.dto.Debtor6Request;
import com.testrestapidct.example.dto.ErrorDtoResponse;
import com.testrestapidct.example.dto.MessageError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Service
public class ExampleServiceImpl {

    public ResponseEntity<Object> validationTest(Debtor6Request debtor6RequestDTO, BindingResult bindingResult){

      try{
          if (bindingResult.hasErrors()) {
              List<ErrorDtoResponse> errors = new ArrayList<>();
              for (FieldError error : bindingResult.getFieldErrors()) {
                  ErrorDtoResponse dto = new ErrorDtoResponse();
                  dto.setPageName("Identity Of Debtor");

                  Field field = Debtor6Request.class.getDeclaredField(error.getField());
                  CustomLabel customLabelAnnotation = field.getAnnotation(CustomLabel.class);
                  String labelName = (customLabelAnnotation != null) ? customLabelAnnotation.value() : error.getField();

                  dto.setLabelName(labelName);
                  dto.setMessage(error.getDefaultMessage());
                  errors.add(dto);
              }

              MessageError message = new MessageError();
              message.setInfo(errors);
              message.setError("Errors");
              return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
          }

          return new ResponseEntity<>("Successfully", HttpStatus.OK);

      }catch (Exception e){
          return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);

      }
}
}
