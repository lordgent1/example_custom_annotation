package com.testrestapidct.example.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Entity
@Setter
@Getter
public class DebtorIdInfo6 {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ALAMAT_USAHA_BISNIS")
    private String alamatUsahaBisnis;

    @Column(name = "KELURAHAN")
    private String kelurahan;

    @Column(name = "KECAMATAN")
    private String kecamatan;

    @Column(name = "DATI_II")
    private String dati2;

    @Column(name = "KODE_POS")
    private String kodePos;

    @Column(name = "NO_TELP_USAHA")
    private String noTelpUsaha;

    @Column(name = "CP")
    private String contactPerson;

    @Column(name = "NO_CP")
    private String noContactPerson;

    @Column(name = "NO_FAX")
    private String noFax;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "NO_SIUP")
    private String noSiup;

    @Column(name = "NO_TDP")
    private String noTdp;

    @Column(name = "NO_AKTE_AWAL")
    private String noAkteAwal;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "TGL_AKTE_AWAL")
    private Date tglAkteAwal;

    @Column(name = "NO_AKTE_AKHIR")
    private String noAkteAkhir;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "TGL_AKTE_AKHIR")
    private Date tglAkteAkhir;

    @Column(name = "NAMA_IBU_KDG")
    private String namaIbuKandung;

    @Column(name = "NPWP")
    private String npwp;

    @Column(name = "KTP")
    private String ktp;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "MASA_BERLAKU_KTP")
    private Date masaBerlakuKtp;

    @Column(name = "ENTITAS_HUKUM")
    private String entitasHukum;
    //

    @Column(name = "KLASIFIKASI")
    private String klasifikasi;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "TGL_KLASIFIKASI")
    private Date tglKlasifikasi;

    /* DCIF - Debitur Individu (hanya utk perorangan) */
    @Column(name = "JML_TANGGUNGAN")
    private Integer jmlTanggungan;

    @Column(name = "KODE_HUB_DGN_LK")
    private String kodeHubDenganLk;

    @Column(name = "NIK_PASSPORT_PSGN")
    private String nikPassportPsgn;

    @Column(name = "NAMA_PSGN")
    private String namaPasangan;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "TGL_LAHIR_PSGN")
    private Date tglLahirPsgn;

    @Column(name = "PERJ_PISAH_HARTA")
    private String perjPisahHarta;

    @Column(name = "MELANGGAR_BMPK")
    private String melanggarBmpk;

    @Column(name = "MELAMPAUI_BMPK")
    private String melampauiBmpk;

    /*
     * Debtor Identity General Information page 6
     * DCIF - Debitur Individu (hanya untuk PT/CV)
     * sampai
     * */


    @Column(name = "KD_JNS_BDN_USAHA")
    private String kodeJnsBdnUsaha;

    @Column(name = "TEMPAT_PENDIRIAN")
    private String tempatPendirian;

    @Column(name = "KD_BIDANG_USAHA")
    private String kodeBidangUsaha;

    @Column(name = "KD_HUB_DGN_LJK")
    private String kdHubDgnLjk;

    @Column(name = "MELANGGAR_BMPKCV")
    private String melanggarBmpkCv;

    @Column(name = "MELAMPAUI_BMPKCV")
    private String melampauiBmpkCV;

    @Column(name = "GO_PUBLIC")
    private String goPublic;

    @Column(name = "KODE_GOL_DEBITUR")
    private String kodeGolDebitur;

    @Column(name = "PERINGKAT_DEBITUR")
    private String peringkatDebitur;

    @Column(name = "LEMBAGA_PEMERINGKAT")
    private String lembagaPemeringkat;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "TGL_PEMERINGKATAN")
    private Date tglPemeringkatan;

    @Column(name = "NAMA_GROUP_DEBITUR")
    private String namaGroupDebitur;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "DATA_AS_OF")
    private Date dataAsOf;

    @Column(name = "ASET")
    private Double aset;

    @Column(name = "ASET_LANCAR")
    private Double asetLancar;

    @Column(name = "KAS_DAN_SETARAKAS")
    private Double kasDansetaraKas;

    @Column(name = "PIUTANG_USAHA_PEMBIAYAAN1")
    private Double piutangUsaha1;

    @Column(name = "INVESTASI1")
    private Double investasi1;

    @Column(name = "ASET_LANCAR_LAINNYA")
    private Double asetLancarLainnya;

    @Column(name = "ASET_TIDAK_LANCAR")
    private Double asetTidakLancar;

    @Column(name = "PIUTANG_USAHA_PEMBIAYAAN2")
    private Double piutangUsaha2;

    @Column(name = "INVESTASI2")
    private Double investasi2;

    @Column(name = "ASET_TIDAK_LANCAR_LAINNYA")
    private Double asetTidakLancarLainnya;


    @Column(name = "LIABILITAS")
    private Double liabilitas;

    @Column(name = "LIABILITAS_JANGKA_PENDEK")
    private Double liabilitasJangkaPendek;

    @Column(name = "PINJAMAN_JANGKA_PENDEK")
    private Double pinjamanJangkaPendek;

    @Column(name = "UTANG_USAHA1")
    private Double utangUsaha1;

    @Column(name = "LIAJANGKA_PENDEK_LAINNYA")
    private Double liaJangkaPendekLainnya;

    @Column(name = "LIAJANGKA_JANGKA_PANJANG")
    private Double liaJangkaPanjang;

    @Column(name = "PINJAMAN_JANGKA_PANJANG")
    private Double pinjJangkaPanjang;

    @Column(name = "UTANG_USAHA2")
    private Double utangUsaha2;

    @Column(name = "LIALIBILITAS_JANGKAPANJANG_LAINNYA")
    private Double liajangkaPanjangLain;

    @Column(name = "EKUITAS")
    private Double ekuitas;

    @Column(name = "PENDAPATAN_USAHA")
    private Double pendapatanUsaha;

    @Column(name = "BEBAN_POKOK")
    private Double bebanPokok;

    @Column(name = "LABA_RUGI")
    private Double labaRugi;

    @Column(name = "PENDAPATAN_LAIN")
    private Double pendapatanLain;

    @Column(name = "BEBAN_LAIN_LAIN")
    private Double bebanLain;

    @Column(name = "LABARUGI_SBLM_PAJAK")
    private Double labaRugiSblmPajak;

    @Column(name = "LABARUGI_PERIODE")
    private Double labarugiPeriode;

    @Column(name = "KODE_JENIS_GUNA")
    private String kodeJenisGuna;

    @Column(name = "KODE_ORIENTASI_GUNA")
    private String kodeOrientasiGuna;

    @Column(name = "KREDIT_PROGRAM_PEM")
    private String kreditProPemerintah;

    @Column(name = "TAKE_OVER_DARI")
    private String takeOverDari;

    @Column(name = "KODE_SEBAB_MACET")
    private String kodeSebabMacet;

    @Column(name = "NILAI_AGUNAN")
    private Double nilaiAgunan;

    /* Data tambahan dalam rangka antasena */
    @Column(name = "LEVEL1")
    private String level1;

    @Column(name = "LEVEL2")
    private String level2;

    @Column(name = "LEVEL3")
    private String level3;

    @Column(name = "LEVEL4")
    private String level4;

    @Column(name = "LEVEL5")
    private String level5;

    @Column(name = "LEVEL6")
    private String level6;

    @Column(name = "LEVEL7")
    private String level7;

    @Column(name = "LEVEL8")
    private String level8;

    @Column(name = "LBU")
    private String lbu;

    @Column(name = "FK_IDAATEMPLATE")
    private String idAaTemplate;

}
