package com.testrestapidct.example.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class MessageError {
    private String error;
    private List<ErrorDtoResponse> info;
}
